Feature: Register merchant account
    Scenario: Register merchant success
        Given a Merchant with first name "Elon" last name "Musk" cpr "1234" bankAccount "4321"
        When merchant is registered
        Then the Merchant "id" not empty
    
    Scenario: Register duplicate merchant
        Given a Merchant with first name "Jeff" last name "Bezos" cpr "4321" bankAccount "5678"
        When merchant is registered
        Given a Merchant with first name "Jeff" last name "Bezos" cpr "4321" bankAccount "5678"
        When merchant is registered
        Then a error message is returned saying "The merchant already exists" 

    Scenario: Deregister merchant success
        Given a Merchant with first name "Elon" last name "Musk" cpr "1234" bankAccount "4321"
        When merchant is registered
        When merchant is deregistered
        Then the merchant does not exist

    Scenario: Deregister merchant that does not exist
        Given a fake id "fakeId"
        When merchant is deregistered
        Then a error message is returned saying "The merchant with id: fakeId does not exists"
