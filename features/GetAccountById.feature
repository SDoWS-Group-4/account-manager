Feature: Get an account by id
    Scenario: Get a customer by id succesfully
        Given a customer with id "1234" and first name "Elon" last name "Musk" cpr "1234" bankAccount "4321"
        When a customer is requested by id
        Then a customer with id "1234" is returned

    Scenario: Get a merchant by id succesfully
        Given a merchant with id "1234" and first name "Elon" last name "Musk" cpr "1234" bankAccount "4321"
        When a merchant is requested by id
        Then a merchant with id "1234" is returned

    Scenario: Customer does not exist
        Given a customer with id "doesNotExist"
        When a customer is requested by id
        Then an error message is returned saying "The customer with id: doesNotExist does not exists"

    Scenario: Merchant does not exist
        Given a merchant with id "doesNotExist"
        When a merchant is requested by id
        Then an error message is returned saying "The merchant with id: doesNotExist does not exists"