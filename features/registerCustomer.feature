Feature: Register customer account
    Scenario: Register customer success
        Given a Customer with first name "Elon" last name "Musk" cpr "1234" bankAccount "4321"
        When customer is registered
        Then the Customer "id" not empty
    
    Scenario: Register duplicate customer
        Given a Customer with first name "Jeff" last name "Bezos" cpr "4321" bankAccount "5678"
        When customer is registered
        Given a Customer with first name "Jeff" last name "Bezos" cpr "4321" bankAccount "5678"
        When customer is registered
        Then a error message is returned saying "The customer already exists" 

    Scenario: Deregister customer success
        Given a Customer with first name "Elon" last name "Musk" cpr "1234" bankAccount "4321"
        When customer is registered
        When customer is deregistered
        Then the customer does not exist

    Scenario: Deregister customer that does not exist
        Given a fake id "fakeId"
        When customer is deregistered
        Then a error message is returned saying "The customer with id: fakeId does not exists"

