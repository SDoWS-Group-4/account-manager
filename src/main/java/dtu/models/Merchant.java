package dtu.models;

import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Data
@EqualsAndHashCode
public class  Merchant implements Serializable{
	
	private static final long serialVersionUID = 9023222981284806610L;
	@Getter @Setter private String id;
	@Getter @Setter private String firstName;
	@Getter @Setter private String lastName;
	@Getter @Setter private String cprNumber;
	@Getter @Setter private String bankAccount;
	
	@Override
	public String toString() {
		return String.format("Merchant name %s, id %s", firstName, id);
	}
}
