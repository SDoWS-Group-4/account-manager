package dtu.services;

import java.util.NoSuchElementException;

import dtu.managers.CustomerManager;
import dtu.models.CorrelationId;
import dtu.models.Customer;
import messaging.Event;
import messaging.MessageQueue;

public class CustomerService {
	CustomerManager customerManager = new CustomerManager();
	MessageQueue queue;

	public CustomerService(MessageQueue q) {
		this.queue = q;
		this.queue.addHandler("CustomerRegistrationRequested", this::handleCustomerRegistrationRequested);
		this.queue.addHandler("CustomerDeregistrationRequested", this::handleCustomerDeregistrationRequested);
		this.queue.addHandler("CustomerByIdRequested", this::handleCustomerByIdRequested);

		this.queue.addHandler("CustomerByDTUIdRequested", this::handleCustomerByDTUIdRequested);

	}

	public void handleCustomerRegistrationRequested(Event ev) {
		var customer = ev.getArgument(0, Customer.class);
		var correlationId = ev.getArgument(1, CorrelationId.class);
		Event event;
		try {
			String customerId = customerManager.registerCustomer(customer);
			event = new Event("CustomerIdAssigned", new Object[] { customerId, correlationId });
		} catch (IllegalArgumentException exception) {
			exception.printStackTrace();
			event = new Event("CustomerNotAssigned", new Object[] { exception, correlationId });
		}
		queue.publish(event);
	}

	public void handleCustomerDeregistrationRequested(Event ev) {
		var id = ev.getArgument(0, String.class);
		var correlationId = ev.getArgument(1, CorrelationId.class);
		Event event;
		try {
			String response = customerManager.deregisterCustomer(id);
			event = new Event("CustomerDeregistered", new Object[] {response, correlationId });
		} catch (NoSuchElementException exception) {
			event = new Event("CustomerNotDeregistered", new Object[] { exception, correlationId });
		}
		queue.publish(event);
	}

	private void handleCustomerByIdRequested(Event ev) {
		var id = ev.getArgument(0, String.class);
		var correlationId = ev.getArgument(1, CorrelationId.class);
		Event event;
		try {
			Customer customer = customerManager.getCustomerById(id);
			event = new Event("CustomerByIdFound", new Object[] {customer, correlationId });
		} catch (NoSuchElementException exception) {
			event = new Event("CustomerByIdNotFound", new Object[] { exception, correlationId });
		}
		queue.publish(event);
	}


	private void handleCustomerByDTUIdRequested(Event ev) {
		var id = ev.getArgument(0, String.class);
		var correlationId = ev.getArgument(1, CorrelationId.class);
		Event event;
		try {
			Customer customer = customerManager.getCustomerById(id);
			event = new Event("CustomerByDTUIdFound", new Object[] {customer, correlationId });
		} catch (NoSuchElementException exception) {
			event = new Event("CustomerByDTUIdNotFound", new Object[] { exception, correlationId });
		}
		queue.publish(event);
	}
}
