package dtu.services;

import java.util.NoSuchElementException;

import dtu.managers.MerchantManager;
import dtu.models.CorrelationId;
import dtu.models.Merchant;
import messaging.Event;
import messaging.MessageQueue;

public class MerchantService {
	MerchantManager merchantManager = new MerchantManager();
	MessageQueue queue;

	public MerchantService(MessageQueue q) {
		this.queue = q;
		this.queue.addHandler("MerchantRegistrationRequested", this::handleMerchantRegistrationRequested);
		this.queue.addHandler("MerchantDeregistrationRequested", this::handleMerchantDeregistrationRequested);
		this.queue.addHandler("MerchantByIdRequested", this::handleMerchantByIdRequested);
	}

	public void handleMerchantRegistrationRequested(Event ev) {
		var merchant = ev.getArgument(0, Merchant.class);
		var correlationId = ev.getArgument(1, CorrelationId.class);
		Event event;
		try {
			String merchantId = merchantManager.registerMerchant(merchant);
			event = new Event("MerchantIdAssigned", new Object[] { merchantId, correlationId });
		} catch (IllegalArgumentException exception) {
			exception.printStackTrace();
			event = new Event("MerchantNotAssigned", new Object[] { exception, correlationId });
		}
		queue.publish(event);
	}

	public void handleMerchantDeregistrationRequested(Event ev) {
		var id = ev.getArgument(0, String.class);
		var correlationId = ev.getArgument(1, CorrelationId.class);
		Event event;
		try {
			String response = merchantManager.deregisterMerchant(id);
			event = new Event("MerchantDeregistered", new Object[] { response, correlationId });
		} catch (NoSuchElementException exception) {
			event = new Event("MerchantNotDeregistered", new Object[] { exception, correlationId });
		}
		queue.publish(event);
	}

	private void handleMerchantByIdRequested(Event ev) {
		var id = ev.getArgument(0, String.class);
		var correlationId = ev.getArgument(1, CorrelationId.class);
		Event event;
		try {
			Merchant merchant = merchantManager.getMerchantById(id);
			event = new Event("MerchantByIdFound", new Object[] { merchant, correlationId });
		} catch (NoSuchElementException exception) {
			event = new Event("MerchantByIdNotFound", new Object[] { exception, correlationId });
		}
		queue.publish(event);
	}
}
