package dtu.managers;

import java.util.NoSuchElementException;
import java.util.UUID;

import dtu.AccountStore;
import dtu.models.Merchant;

public class MerchantManager {

    AccountStore accountStore = AccountStore.getInstance();

    public String registerMerchant(Merchant merchant) throws IllegalArgumentException {

        if (merchantExists(merchant)) {
            throw new IllegalArgumentException("The merchant already exists");
        }

        merchant.setId(randomId());
        accountStore.addMerchant(merchant);
        return merchant.getId();
    }

    public String deregisterMerchant(String id) throws NoSuchElementException {
        if (!merchantExists(id)) {
            throw new NoSuchElementException("The merchant with id: " + id + " does not exists");
        }

        accountStore.removeMerchant(id);
        return "Merchant with id: " + id + " was succesfully deleted";
    }

    private String randomId() {
        return UUID.randomUUID().toString();
    }

    private Boolean merchantExists(Merchant merchant) {
        return accountStore.getMerchants().stream()
                .anyMatch(m -> m.getCprNumber().equals(merchant.getCprNumber()));
    }

    private Boolean merchantExists(String merchantId) {
        return accountStore.getMerchants().stream()
                .anyMatch(m -> m.getId().equals(merchantId));
    }

    public Merchant getMerchantById(String id) {
        try {
            return accountStore.getMerchants().stream()
                    .filter(m -> m.getId().equals(id)).findFirst().get();
        } catch (NullPointerException | NoSuchElementException e) {
            throw new NoSuchElementException("The merchant with id: " + id + " does not exists");
        }
    }
}
