package dtu.managers;

import java.util.NoSuchElementException;
import java.util.UUID;

import dtu.AccountStore;
import dtu.models.Customer;

public class CustomerManager {

    AccountStore accountStore = AccountStore.getInstance();

    public String registerCustomer(Customer customer) throws IllegalArgumentException {

        if (customerExists(customer)) {
            throw new IllegalArgumentException("The customer already exists");
        }

        customer.setId(randomId());
        accountStore.addCustomer(customer);
        return customer.getId();
    }

    public String deregisterCustomer(String id) throws NoSuchElementException {
        if (!customerExists(id)) {
            throw new NoSuchElementException("The customer with id: " + id + " does not exists");
        }

        accountStore.removeCustomer(id);
        return "Customer with id: " + id + " was succesfully deleted";
    }

    private String randomId() {
        return UUID.randomUUID().toString();
    }

    private Boolean customerExists(Customer customer) {
        return accountStore.getCustomers().stream()
                .anyMatch(c -> c.getCprNumber().equals(customer.getCprNumber()));
    }

    private Boolean customerExists(String customerId) {
        return accountStore.getCustomers().stream()
                .anyMatch(c -> c.getId().equals(customerId));
    }

    public Customer getCustomerById(String id) {
        try {
            return accountStore.getCustomers().stream()
                    .filter(c -> c.getId().equals(id)).findFirst().get();
        } catch (NullPointerException | NoSuchElementException e) {
            throw new NoSuchElementException("The customer with id: " + id + " does not exists");
        }
    }
}
