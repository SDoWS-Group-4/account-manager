package dtu;

import java.util.ArrayList;
import java.util.List;

import dtu.models.Customer;
import dtu.models.Merchant;

public class AccountStore {
    private List<Customer> customers = new ArrayList<>();
    private List<Merchant> merchants = new ArrayList<>();

    private static AccountStore single_instance = null;

    AccountStore() {
        Customer tokenTestCustomer = new Customer();
        tokenTestCustomer.setFirstName("TokenTest");
        tokenTestCustomer.setLastName("TokenTest");
        tokenTestCustomer.setCprNumber("TokenTest");
        tokenTestCustomer.setId("TokenTest");
        tokenTestCustomer.setBankAccount("TokenTest");
        customers.add(tokenTestCustomer);
    }

    public List<Customer> getCustomers() {
        return customers;
    }

    public List<Merchant> getMerchants() {
        return merchants;
    }

    public void addCustomer(Customer customer) {
       customers.add(customer);
    }

    public void removeCustomer(String id) {
        customers.removeIf(c -> c.getId().equals(id));
    }

    public void setMerchants(List<Merchant> merchants) {
        this.merchants = merchants;
    }
    
    public void addMerchant(Merchant merchant) {
        merchants.add(merchant);
    }
    
    public void removeMerchant(String id) {
        merchants.removeIf(m -> m.getId().equals(id));
    }

    public void removeAllCustomers(){
        customers.clear();
    }
    
    public void removeAllMerchants(){
        merchants.clear();
    }

    public void reset() {
        customers.clear();
        merchants.clear();
        Customer tokenTestCustomer = new Customer();
        tokenTestCustomer.setFirstName("TokenTest");
        tokenTestCustomer.setLastName("TokenTest");
        tokenTestCustomer.setCprNumber("TokenTest");
        tokenTestCustomer.setId("TokenTest");
        customers.add(tokenTestCustomer);
    }
    
    public static AccountStore getInstance() {
        if (single_instance == null)
            single_instance = new AccountStore();

        return single_instance;
    }
}
