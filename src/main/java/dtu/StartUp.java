package dtu;

import dtu.services.CustomerService;
import dtu.services.MerchantService;
import messaging.implementations.RabbitMqQueue;

public class StartUp {
	public static void main(String[] args) throws Exception {
		new StartUp().startUp();
	}

	private void startUp() throws Exception {
		System.out.println("startup");
		var mq = new RabbitMqQueue("rabbitmq");
		// AccountStore accountStore = new AccountStore();
		new MerchantService(mq);
		new CustomerService(mq);
	}
}
