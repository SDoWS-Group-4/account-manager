package behaviourtests;

import static org.junit.jupiter.api.Assertions.assertEquals;

import dtu.AccountStore;
import dtu.managers.CustomerManager;
import dtu.managers.MerchantManager;
import dtu.models.Customer;
import dtu.models.Merchant;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class GetAccountByIDSteps {
    CustomerManager customerManager = new CustomerManager();
    MerchantManager merchantManager = new MerchantManager();
    AccountStore accountStore = AccountStore.getInstance();
    Customer customer;
    Customer tempCustomer;
    Merchant merchant;
    Merchant tempMerchant;
    String id;
    Exception exception;

    @Before
    public void setup() {
        accountStore.reset();
    }

    @Given("a customer with id {string} and first name {string} last name {string} cpr {string} bankAccount {string}")
    public void aCustomerWithIdAndFirstNameLastNameCprBankAccount(String id, String firstName, String lastName,
            String cpr, String bankAccount) {
        customer = new Customer();
        customer.setFirstName(firstName);
        customer.setLastName(lastName);
        customer.setCprNumber(cpr);
        customer.setBankAccount(bankAccount);
        customer.setId(id);
        accountStore.addCustomer(customer);
    }

    @When("a customer is requested by id")
    public void aCustomerIsRequestedById() {
        try {
            tempCustomer = new Customer();
            tempCustomer = customerManager.getCustomerById(customer.getId());
        } catch (Exception e) {
            exception = e;
        }
    }

    @When("a merchant is requested by id")
    public void aMerchantIsRequestedById() {
        try {
            tempMerchant = new Merchant();
            tempMerchant = merchantManager.getMerchantById(merchant.getId());
        } catch (Exception e) {
            exception = e;
        }
    }

    @Then("a customer with id {string} is returned")
    public void aCustomerWithIdIsReturned(String id) {
        assertEquals(id, tempCustomer.getId());
    }

    @Given("a merchant with id {string} and first name {string} last name {string} cpr {string} bankAccount {string}")
    public void aMerchantWithIdAndFirstNameLastNameCprBankAccount(String id, String firstName, String lastName,
            String cpr, String bankAccount) {
        merchant = new Merchant();
        merchant.setFirstName(firstName);
        merchant.setLastName(lastName);
        merchant.setCprNumber(cpr);
        merchant.setBankAccount(bankAccount);
        merchant.setId(id);

        accountStore.addMerchant(merchant);
    }

    @Then("a merchant with id {string} is returned")
    public void aMerchantWithIdIsReturned(String id) {
        // Write code here that turns the phrase above into concrete actions
        assertEquals(id, tempMerchant.getId());
    }

    @Given("a customer with id {string}")
    public void aCustomerWithId(String id) {
        this.customer = new Customer();
        this.customer.setId(id);
    }

    @Then("an error message is returned saying {string}")
    public void anErrorMessageIsReturnedSaying(String errormsg) {
        assertEquals(errormsg, exception.getMessage());
    }

    @Given("a merchant with id {string}")
    public void aMerchantWithId(String id) {
        this.merchant = new Merchant();
        this.merchant.setId(id);
    }

    @After
    public void tearDown() {
        accountStore.reset();
    }
}
