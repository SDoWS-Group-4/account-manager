package behaviourtests;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.Arrays;

import org.junit.Assert;

import dtu.AccountStore;
import dtu.managers.CustomerManager;
import dtu.managers.MerchantManager;
import dtu.models.Customer;
import dtu.models.Merchant;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class RegisterSteps {
    CustomerManager customerManager = new CustomerManager();
    MerchantManager merchantManager = new MerchantManager();
    AccountStore accountStore = AccountStore.getInstance();
    Customer customer;
    Merchant merchant;
    String id;
    Exception exception;

    @Before
    public void setup() {
        accountStore.reset();
    }

    @Given("a Customer with first name {string} last name {string} cpr {string} bankAccount {string}")
    public void aCustomerWithFirstNameLastNameCprBankAccount(String firstName, String lastName, String cpr,
            String bankAccount) {
        customer = new Customer();
        customer.setFirstName(firstName);
        customer.setLastName(lastName);
        customer.setCprNumber(cpr);
        customer.setBankAccount(bankAccount);
    }

    @When("customer is registered")
    public void customerIsRegistered() {
        try {
            customer.setId(customerManager.registerCustomer(customer));
        } catch (Exception e) {
            exception = e;
        }
    }

    @Then("the Customer {string} not empty")
    public void theCustomerNotEmpty(String string) {
        assertFalse(customer.getId().isEmpty());
    }

    @Then("a error message is returned saying {string}")
    public void aErrorMessageIsReturnedSaying(String errormsg) {
        assertEquals(errormsg, exception.getMessage());
    }

    @When("customer is deregistered")
    public void customerIsDeregistered() {
        try {
            customerManager.deregisterCustomer(customer.getId());
        } catch (Exception e) {
            exception = e;
        }
    }

    @Then("the customer does not exist")
    public void theCustomerDoesNotExist() {
        assertFalse(accountStore.getCustomers()
                .stream()
                .anyMatch(c -> c.getId()
                        .equals(customer.getId())));
    }

    @Given("a fake id {string}")
    public void aFakeId(String fakeId) {
        customer = new Customer();
        customer.setId(fakeId);

        merchant = new Merchant();
        merchant.setId(fakeId);
    }

    @Given("a Merchant with first name {string} last name {string} cpr {string} bankAccount {string}")
    public void aMerchantWithFirstNameLastNameCprBankAccount(String firstName, String lastName, String cpr,
            String bankAccount) {
        merchant = new Merchant();
        merchant.setFirstName(firstName);
        merchant.setLastName(lastName);
        merchant.setCprNumber(cpr);
        merchant.setBankAccount(bankAccount);
    }

    @When("merchant is registered")
    public void merchantIsRegistered() {
        try {
            merchant.setId(merchantManager.registerMerchant(merchant));
        } catch (Exception e) {
            exception = e;
        }
    }

    @Then("the Merchant {string} not empty")
    public void theMerchantNotEmpty(String string) {
        assertFalse(merchant.getId().isEmpty());
    }

    @When("merchant is deregistered")
    public void merchantIsDeregistered() {
        try {
            merchantManager.deregisterMerchant(merchant.getId());
        } catch (Exception e) {
            exception = e;
        }
    }

    @Then("the merchant does not exist")
    public void theMerchantDoesNotExist() {
        assertFalse(accountStore.getMerchants()
                .stream()
                .anyMatch(c -> c.getId()
                        .equals(merchant.getId())));
    }

    @After
    public void tearDown() {
        accountStore.reset();

    }
}
